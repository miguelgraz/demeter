# Demeter

A proposed solution for Trecker code challenge. The name comes from the Greek goddess of agriculture.

## Dependencies and setup

It is a Sinatra project using Bundler to manage the few gem dependencies, it depends on having Git, Ruby 2.4.2 and Postgres installed and running.
To start playing with it you can just run:

1. `git clone git@bitbucket.org:miguelgraz/demeter.git`
2. `cd demeter`
3. `bundle`
4. `rake db:create db:migrate`
5. `rackup`

This will fire up the server and expose the API on http://localhost:9292.

The system currently has only two methods:

* `post /record` to receive the JSON params and store a driver (if not yet stored) and a record for that driver
* `get /list` to render a very simple HTML with the list of activities per day per driver

Unfortunately the management of fields couldn't be implemented dynamically so for now it only has a `FIELDS` constant being an array of groups of points representing the fields.

## Test suite

The project has a test suite built using RSpec that should be installed by running `bundle`, the tests can be executed by simply running `rspec`.

## Questions and Reasons

### Drivers and Companies

It'd be reasonable to think that companies and drivers should be stored in different tables but given that the challenge doesn't use companies for anything else than description and that the sample data is unknow I didn't want to add unnecessary complexity or enforce normalized data on the test sample, I chose to accept all data regardless of the relationship company <> driver.

### Speed

As the description uses km for speed the system assumes that the `speed` sent on the post request is also in km.

### Tests manually destroying the test database

To avoid adding yet another gem to do it, seemed enough for the test.

### Records that doesn't fall in any explained category?

Records that are not part of predefined fields can only be classified as `Driving` if the speed is more than 5km/h, at the same time the example output doesn't show what happened during one specific hour, because of that I'm assuming that these "unclassified records" will have the power to stop the current activity but won't be compiled as any other activity on the list.

## Improvements

Move the geofencing calculation to a background job so its processing wouldn't be part of the web request, making the request cycle as lean as possible so the server would be able to handle more per second.

Store the minimum and maximum longitude and latitude for each point to avoid calculating them every time.

Improve the "point within an area" calculations to cover more complex polygons than just rectangles.

Make the list of activities per driver easier to read.

Improve the front end for the list of activities.
