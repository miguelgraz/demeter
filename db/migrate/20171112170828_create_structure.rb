class CreateStructure < ActiveRecord::Migration[5.1]
  def change
    create_table :drivers do |t|
      t.integer :company_id, index: true

      t.timestamps
    end

    create_table :records do |t|
      t.references :driver, index: true, foreign_key: true, null: false
      t.decimal :lat, precision: 10, scale: 6, null: false
      t.decimal :lng, precision: 10, scale: 6, null: false
      t.datetime :timestamp, null: false
      t.decimal :accuracy, precision: 5, scale: 2
      t.decimal :speed, precision: 5, scale: 2, null: false
      t.boolean :geofenced, default: false

      t.timestamps
    end
  end
end
