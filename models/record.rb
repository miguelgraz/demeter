class Record < ActiveRecord::Base
  belongs_to :driver

  validates :driver_id, :lat, :lng, :timestamp, :speed, presence: true

  before_create :check_geofencing

  def check_geofencing
    FIELDS.each do |field|
      min_lat = field.min_by { |p| p[:lat] }[:lat]
      max_lat = field.max_by { |p| p[:lat] }[:lat]
      min_lng = field.min_by { |p| p[:lng] }[:lng]
      max_lng = field.max_by { |p| p[:lng] }[:lng]
      if lat > min_lat && lat < max_lat && lng > min_lng && lng < max_lng
        self.geofenced = true
        break
      end
    end
  end

  def unclassified?
    speed <= 5 && ! geofenced
  end

  def category
    return if unclassified?
    geofenced? ? (speed > 1 ? 'Cultivating' : 'Repairing') : 'Driving'
  end
end
