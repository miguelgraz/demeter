class Driver < ActiveRecord::Base
  has_many :records, dependent: :destroy

  def activities
    result = { driver_id: id, dates: [] }
    current_date = { activities: [] }
    current_activity = nil
    last_date = nil
    last_record = nil

    records.order(:timestamp).each do |rec|
      last_record = rec.timestamp

      if rec.timestamp.strftime('%d/%m/%Y') != last_date
        # The day has changed, close any open activity, close the day and prepare the next
        if current_activity.present?
          current_date[:activities] << close_activity(current_activity, rec.timestamp.midnight)
        end
        result[:dates] << current_date unless last_date.nil?
        current_date = { activities: [] }
        current_activity = nil
        current_date[:day] = last_date = rec.timestamp.strftime('%d/%m/%Y')
      end

      current_activity ||= start_activity(rec)
      next if rec.category == current_activity[:desc]
      current_date[:activities] << close_activity(current_activity, rec.timestamp)
      current_activity = start_activity(rec)
    end

    current_date[:activities] << close_activity(current_activity, last_record.midnight) if current_activity.present?
    result[:dates] << current_date
    result
  end

  private

  def start_activity(record)
    record.unclassified? ? nil : { desc: record.category, from: record.timestamp }
  end

  def close_activity(activity, final)
    activity[:time] = Time.at(final - activity[:from]).utc.strftime("%T")
    activity[:from] = activity[:from].strftime('%T')
    activity[:to] = final.strftime('%T')
    activity
  end
end
