require 'bundler'
Bundler.require

require './models/driver'
require './models/record'

FIELDS = [
  [
    { lat: 52.523654, lng: 13.412168 },
    { lat: 52.519933, lng: 13.412973 },
    { lat: 52.520070, lng: 13.405849 },
    { lat: 52.517975, lng: 13.408692 }
  ],
  [
    { lat: 52.515377, lng: 13.349296 },
    { lat: 52.509902, lng: 13.350237 },
    { lat: 52.512066, lng: 13.373052 },
    { lat: 52.518002, lng: 13.371715 }
  ]
]

class Demeter < Sinatra::Application
  post '/record' do
    content_type :json

    Driver.find_or_create_by(id: params[:driver_id], company_id: params[:company_id]) if params[:driver_id]
    record = Record.new(
      driver_id: params[:driver_id],
      timestamp: params[:timestamp],
      lat: params[:latitude],
      lng: params[:longitude],
      accuracy: params[:accuracy],
      speed: params[:speed]
    )

    if record.save
      halt 204
    else
      halt 422, record.errors.full_messages.to_json
    end
  end

  get '/list' do
    @drivers = Driver.all.map(&:activities)
    erb :list
  end
end
