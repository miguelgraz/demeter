require File.expand_path '../../spec_helper.rb', __FILE__

describe "Driver" do
  describe 'list of activities' do
    subject { Driver.create }

    describe 'is properly categorized' do
      let!(:driving1) { Record.create(driver_id: subject.id, lat: 53.527225, lng: 12.433896, timestamp: '2017-11-20T08:12:38', speed: 7.0) }
      let!(:driving2) { Record.create(driver_id: subject.id, lat: 53.123, lng: 12.456, timestamp: '2017-11-20T07:47:32', speed: 8.0) }
      let!(:cultivating1) { Record.create(driver_id: subject.id, lat: 52.514955, lng: 13.367532, timestamp: '2017-11-20T09:50:40', speed: 1.5) }
      let!(:unclassified1) { Record.create(driver_id: subject.id, lat: 52.509334, lng: 13.411305, timestamp: '2017-11-20T11:00:00', speed: 0.0) }
      let!(:repairing1) { Record.create(driver_id: subject.id, lat: 52.522220, lng: 13.412216, timestamp: '2017-11-20T15:00:00', speed: 0.3) }
      let!(:unclassified2) { Record.create(driver_id: subject.id, lat: 52.470328, lng: 13.410432, timestamp: '2017-11-20T18:30:45', speed: 0.0) }

      it 'has start, end and total_time' do
        expected = {
          driver_id: subject.id,
          dates: [{
            day: '20/11/2017',
            activities: [
              {
                desc: 'Driving',
                from: '07:47:32',
                to: '09:50:40',
                time: '02:03:08'
              },
              {
                desc: 'Cultivating',
                from: '09:50:40',
                to: '11:00:00',
                time: '01:09:20'
              },
              {
                desc: 'Repairing',
                from: '15:00:00',
                to: '18:30:45',
                time: '03:30:45'
              }
            ]
          }]
        }
        expect(subject.activities).to eq expected
      end
    end

    describe 'is separated by day' do
      context "when an unclassified record starts and there's nothing after it" do
        let!(:cultivating1) { Record.create(driver_id: subject.id, lat: 52.514955, lng: 13.367532, timestamp: '2017-11-21T09:50:40', speed: 1.5) }
        let!(:unclassified1) { Record.create(driver_id: subject.id, lat: 52.509334, lng: 13.411305, timestamp: '2017-11-21T17:00:00', speed: 0.0) }

        it 'ends the day by the end of the last classified record' do
          expected = {
            driver_id: subject.id,
            dates: [{
              day: '21/11/2017',
              activities: [
                {
                  desc: 'Cultivating',
                  from: '09:50:40',
                  to: '17:00:00',
                  time: '07:09:20'
                }
              ]
            }]
          }

          expect(subject.activities).to eq expected
        end
      end

      context "when an unclassified record starts and there's nothing after it" do
        let!(:driving1) { Record.create(driver_id: subject.id, lat: 53.527225, lng: 12.433896, timestamp: '2017-11-22T08:12:38', speed: 7.0) }
        let!(:repairing1) { Record.create(driver_id: subject.id, lat: 52.521614, lng: 13.409716, timestamp: '2017-11-23T09:50:40', speed: 0.2) }

        it "have more than one day and ends the day at midnight" do
          expected = {
            driver_id: subject.id,
            dates: [
              {
                day: '22/11/2017',
                activities: [
                  {
                    desc: 'Driving',
                    from: '08:12:38',
                    to: '00:00:00',
                    time: '15:47:22'
                  }
                ]
              },
              {
                day: '23/11/2017',
                activities: [
                  {
                    desc: 'Repairing',
                    from: '09:50:40',
                    to: '00:00:00',
                    time: '14:09:20'
                  }
                ]
              }
            ]
          }

          expect(subject.activities).to eq expected
        end
      end
    end
  end
end
