require File.expand_path '../../spec_helper.rb', __FILE__

describe "Record" do
  describe 'validations' do
    subject { Record.new(driver_id: 6, lat: 54.3759, lng: 13.8258, timestamp: '2017-11-12T22:12:32', speed: 8.0) }

    it 'is valid with all attributes' do
      expect(subject).to be_valid
    end
    it 'it not valid without driver' do
      subject.driver_id = nil
      expect(subject).not_to be_valid
    end
    it 'it not valid without lat' do
      subject.lat = nil
      expect(subject).not_to be_valid
    end
    it 'it not valid without lng' do
      subject.lng = nil
      expect(subject).not_to be_valid
    end
    it 'it not valid without timestamp' do
      subject.timestamp = nil
      expect(subject).not_to be_valid
    end
    it 'it not valid without speed' do
      subject.speed = nil
      expect(subject).not_to be_valid
    end
  end

  describe 'check if geofenced when creating' do
    let(:driver) { Driver.create }
    let(:inside_record1) { Record.create(driver_id: driver.id, lat: 52.519666, lng: 13.409100, timestamp: '2017-11-12T22:12:32', speed: 8.0) }
    let(:inside_record2) { Record.create(driver_id: driver.id, lat: 52.512957, lng: 13.362176, timestamp: '2017-11-12T22:12:35', speed: 8.0) }
    let(:outside_record) { Record.create(driver_id: driver.id, lat: 52.527225, lng: 13.433896, timestamp: '2017-11-12T22:12:38', speed: 8.0) }

    it 'marks as geofenced if inside the polygon' do
      expect(inside_record1.geofenced).to be true
      expect(inside_record2.geofenced).to be true
    end

    it 'doesnt mark if outside the polygon' do
      expect(outside_record.geofenced).to be false
    end
  end
end
