ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require File.expand_path '../../demeter.rb', __FILE__

module RSpecMixin
  include Rack::Test::Methods

  def app() Demeter end
end

RSpec.configure do |config|
  config.include RSpecMixin

  config.after :all do
    Record.destroy_all
    Driver.destroy_all
  end
end
