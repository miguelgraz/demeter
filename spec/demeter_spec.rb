require File.expand_path '../spec_helper.rb', __FILE__

describe "Demeter requests" do
  describe 'create a new record' do
    context 'for an existent driver' do
      it 'creates one new record' do
        driver = Driver.create!(id: 1, company_id: 1)
        post_data = {
          company_id: 1,
          driver_id: driver.id,
          timestamp: '2017-11-01T08:14:03',
          latitude: 52.52464,
          longitude: 13.4126,
          accuracy: 10.0,
          speed: 9.0
        }

        expect {
          post "/record", post_data
        }.to change { Record.count }.by(1)
        expect(Driver.count).to eq 1

        expect(last_response.status).to eq 204
        expect(last_response.body).to be_empty
      end
    end

    context 'for a new driver' do
      it 'create a new record and a new driver' do
        post_data = {
          company_id: 2,
          driver_id: 2,
          timestamp: '2017-11-01T08:14:03',
          latitude: 52.52464,
          longitude: 13.4126,
          accuracy: 10.0,
          speed: 9.0
        }

        expect {
          post "/record", post_data
        }.to change { Record.count }.by(1).and change { Driver.count }.by(1)

        expect(last_response.status).to eq 204
        expect(last_response.body).to be_empty
      end
    end

    context 'weird request' do
      it 'returns a sensible error' do
        post_data = { weidness: 'yep, very weird' }

        expect {
          post "/record", post_data
        }.to_not change { Record.count }

        expect(JSON.parse(last_response.body)).to eq ["Driver can't be blank", "Lat can't be blank", "Lng can't be blank", "Timestamp can't be blank", "Speed can't be blank"]
        expect(last_response.status).to eq 422
      end
    end
  end
end
